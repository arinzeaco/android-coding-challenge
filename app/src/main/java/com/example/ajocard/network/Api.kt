package com.example.ajocard.network

import com.example.ajocard.constant.Constant
import com.example.ajocard.response.CardDetails
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.PATCH
import retrofit2.http.Path


interface Api {

    @PATCH("{id}")
    suspend fun getCardDetails(@Path("id") id: String?): Response<CardDetails>

}

