package com.example.ajocard

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.ajocard.ViewModel.CardViewModel
import com.example.ajocard.response.CardDetails
import com.example.ajocard.util.AuthListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    companion object {
        var cardViewModel: CardViewModel? = null

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         cardViewModel = ViewModelProvider(this).get(CardViewModel::class.java)
        cardNumber.setText("45717360")
        get_details.setOnClickListener {
            cardViewModel!!.onLoginButtonClick(cardNumber.getText().toString().trim(),
               this
            )
        }

    }



}
