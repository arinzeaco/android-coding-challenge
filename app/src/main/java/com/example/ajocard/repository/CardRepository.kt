package com.example.ajocard.repository

import com.example.ajocard.network.RetrofitClient
import com.example.ajocard.network.SafeApiRequest
import com.example.ajocard.response.CardDetails

class CardRepository : SafeApiRequest(){

    val call = RetrofitClient

    suspend fun cardNumber(cardNumber: String): CardDetails {
        return apiRequest {call.instance.getCardDetails(cardNumber)}
    }

}
