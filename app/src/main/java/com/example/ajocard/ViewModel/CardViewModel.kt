package com.example.ajocard.ViewModel


import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log
import androidx.lifecycle.ViewModel
import com.example.ajocard.CardDetail
import com.example.ajocard.constant.Constant
import com.example.ajocard.repository.CardRepository
import com.example.ajocard.util.AuthListener
import com.example.ajocard.util.Coroutines
import com.google.gson.Gson
import com.shashank.sony.fancytoastlib.FancyToast
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL


class CardViewModel : ViewModel() {
    private var cardRepository: CardRepository? = null


    init {

        cardRepository = CardRepository()
    }

    var authListener: AuthListener? = null


    fun onLoginButtonClick(cardNumber: String, context: Context){
        authListener?.onStarted()
        // Check if carNumber is null
        if(cardNumber.isEmpty() ){

            FancyToast.makeText(context, "Enter a card Number",
                FancyToast.LENGTH_SHORT,  FancyToast.ERROR, false).show()

            return
        }
        // Check internet connection
        if(!isNetworkAvailable(context)){
            FancyToast.makeText(context, "You have no internet connection Network",
                FancyToast.LENGTH_SHORT,  FancyToast.ERROR, false).show()

            return
        }

        Coroutines.main {
            try {
                val authResponse = cardRepository!!.cardNumber("45717360")

                authResponse.let {
                    authListener?.onSuccess(it)
                    // if successfull more to carddetails page
                   val intent=Intent(context, CardDetail::class.java).apply {
                        putExtra("MY_DATA", Gson().toJson(it))


                    }
                    context.startActivity(intent)
                    return@main
                }


            }catch(e: IOException){
                authListener?.onFailure(e.message!!)
                FancyToast.makeText(context, e.message,
                    FancyToast.LENGTH_SHORT,  FancyToast.ERROR, false).show()

            }
        }

    }
    fun connectedToTheNetwork(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    fun isNetworkAvailable(context: Context): Boolean {

        if (connectedToTheNetwork(context)) {
            Thread(Runnable {

                try {
                    val urlc = URL(Constant.BASE_URL)
                        .openConnection() as HttpURLConnection
                    urlc.setRequestProperty("User-Agent", "Android")
                    urlc.setRequestProperty("Connection", "close")
                    urlc.connectTimeout = 1500
                    urlc.connect()

                } catch (e: IOException) {
                    Log.e("BaseActivity__", "Error checking internet connection", e)
                }
            }).start()
            return true

        } else {
            Log.d("BaseActivity__" ,"No network available!")
        }
        return false
    }





}