package com.example.ajocard.util

import com.example.ajocard.response.CardDetails

interface AuthListener {
    fun onStarted()
    fun onSuccess(cardDetails: CardDetails)
    fun onFailure(message: String)
}