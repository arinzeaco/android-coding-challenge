package com.example.ajocard.ViewModel


import android.app.Application
import android.content.Context
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito.mock

class CardViewModelTest {
    var SUT: CardViewModel? = null
    var apps: Application? = null
    val CardNumber = "cardnumber"

    private lateinit var context: Context

    @Before
    @Throws(Exception::class)
    fun setup() {
        context = mock(Context::class.java)

        SUT = CardViewModel()

    }

    @Test
    @Throws(Exception::class)
    fun onLoginButtonClick() {
        val ac =
            ArgumentCaptor.forClass(Any::class.java)
        val result = SUT!!.onLoginButtonClick(CardNumber, context)

//        Assert.assertThat(
//            ac.value, CoreMatchers.`is`(
//                CoreMatchers.instanceOf(
//                    LoggedInEvent::class.java
//                )
//            )
//        )
    }


    @Test
    @Throws(Exception::class)
    fun isNetworkAvailable() {

        val result: Boolean = SUT!!.connectedToTheNetwork(context)

        Assert.assertThat<Boolean>(result, CoreMatchers.`is`(false))
    }
}
//}
