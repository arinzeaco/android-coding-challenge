# Android Coding Challenge - Card Detail Finder
App shows card's details when a card's number is either typed in or scanned using OCR.
Card detail is gotten from [Binlist](https://binlist.net/)


## Features
* MvvM architecture
* Retorift was used to get Http patch response
* Network request failure was also handled
* Coroutines was used to mange the threadpools
* Toast message libary Fancy Toast was use to dislay response to user

## Improvement
* Room Database can be used to store data
* Rxjava for reactivity
* Better UserInterface

## Author
Obi Christopher

